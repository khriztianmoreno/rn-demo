import React from 'react';
import { Text, View, TextInput, Button, Alert } from 'react-native';
import axios from 'axios'

import Styles from './Styles'

export default class Home extends React.Component {
  constructor () {
    super() 

    this.state = {
      isShowingText: true,
      email: ''
    }

    this.onRegister = this.onRegister.bind(this)
  }

  componentDidMount() {
  }

  onRegister () {
    const { email, password } = this.state;

    const data = {
      name: 'User 1',
      email,
      password
    }

    axios.post('http://192.168.1.53:3030/api/users', data)
      .then((a) => console.log('TOKEN:', a))
      .then((error) => console.log('Fail!!'))
  }

  render() {
    const display = this.state.isShowingText ? 'Intermitente' : ' ';
    return (
      <View style={Styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <Text>{display}</Text>
        <TextInput
          style={{height: 50, width: 100}}
          placeholder="Correo"
          onChangeText={(text) => this.setState({email: text})}
        />
        <TextInput
          style={{height: 50, width: 100}}
          placeholder="Password"
          onChangeText={(text) => this.setState({password: text})}
        />
        <Button
          onPress={this.onRegister}
          title="Press Me"
        />
      </View>
    );
  }
}
